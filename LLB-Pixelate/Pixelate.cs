﻿using LLBML.States;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace LLB_Pixelate
{
    public class Pixelate : MonoBehaviour
    {
		int factor => (GameStates.GetCurrent() != GameState.GAME) ? 0 : Plugin.pixelateFactor.Value;

		void OnRenderImage(RenderTexture src, RenderTexture dst)
		{			
			if (factor == 0)
            {
				Graphics.Blit(src, dst);
            }
            else
            {
				RenderTexture offscreenRT = RenderTexture.GetTemporary(Screen.width / (int)factor, Screen.height / (int)factor, 24);
				offscreenRT.antiAliasing = 1;
				src.filterMode = FilterMode.Point;
				offscreenRT.filterMode = FilterMode.Point;

				Graphics.Blit(src, offscreenRT);
				Graphics.Blit(offscreenRT, dst);

				RenderTexture.ReleaseTemporary(offscreenRT);
			}
			
		}
	}
}
