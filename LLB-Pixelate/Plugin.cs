﻿using BepInEx;
using BepInEx.Configuration;
using LLBML.States;
using LLHandlers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace LLB_Pixelate
{
    [BepInDependency(LLBML.PluginInfos.PLUGIN_ID, BepInDependency.DependencyFlags.HardDependency)]
    [BepInDependency("no.mrgentle.plugins.llb.modmenu", BepInDependency.DependencyFlags.SoftDependency)]
    [BepInPlugin(PluginInfo.PLUGIN_GUID, PluginInfo.PLUGIN_NAME, PluginInfo.PLUGIN_VERSION)]
    public class Plugin : BaseUnityPlugin
    {
        public static ConfigEntry<int> pixelateFactor;
        public ConfigEntry<bool> doOutlines;
 
        void Awake()
        {
            pixelateFactor = Config.Bind<int>("General", "pixelateFactor", 0, new ConfigDescription("Pixelation Factor", new AcceptableValueRange<int>(0, 32)));
            doOutlines = Config.Bind<bool>("General", "doOutlines", true, "Do Ball/Player Outlines (recommended)");
            InvokeRepeating("CheckCameras", 0.1f, 0.1f);
        }

        void Start()
        {
            LLBML.Utils.ModDependenciesUtils.RegisterToModMenu(this.Info, new List<String>()
            {
                "<b>Pixelate Factor</b>",
                "0 - Turn the effect OFF",
                "4-6 recommended Arcade experience",
                "32 (minimum pixel)"
            });
        }
        void CheckCameras()
        {
            foreach (Camera c in Camera.allCameras)
            {
                if (c.gameObject.GetComponent<Pixelate>() == null)
                {
                    c.gameObject.AddComponent<Pixelate>();

                    Debug.Log("Added pixelate shader!");
                };
            }
        }

        void Update()
        {
            if (!doOutlines.Value) return;
            if (GameStates.GetCurrent() != GameState.GAME) return;
            if (World.instance == null) return;
            if (World.instance.playerHandler == null) return;
            if (World.instance.ballHandler == null) return;
            for (int i = 0; i < PlayerHandler.AmountPlayersIngame(); i++)
            {
                if (World.instance.playerHandler.GetPlayerEntity(i) == null)
                {

                }
                else
                {
                    World.instance.playerHandler.GetPlayerEntity(i).SetColorOutlinesColor(Color.black);
                    World.instance.playerHandler.GetPlayerEntity(i).SetColorOutlines(true);
                }
               
            }
            if (!World.instance.ballHandler.NoBallsActivelyInMatch())
            {
                World.instance.ballHandler.GetBall(0).SetColorOutlinesColor(Color.black);
                World.instance.ballHandler.GetBall(0).SetColorOutlines(true);
            }

        }
    }
}
