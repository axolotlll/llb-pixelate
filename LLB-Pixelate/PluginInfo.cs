﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LLB_Pixelate
{
    internal static class PluginInfo
    {
        public const string PLUGIN_GUID = "com.gitlab.axolotlll.llb-pixelate";
        public const string PLUGIN_NAME = "Pixelate";
        public const string PLUGIN_VERSION = "1.0.4";
    }
}
